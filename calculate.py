#!/usr/bin/env python3
"""Computer code for SITP Problem 2.9"""

from astropy.table import Table, Column
from astropy.io import ascii
import scipy.special  # for combinatorial function
import matplotlib.pyplot as plt
import numpy as np


def multiplicity(N, q):
    """Multiplicity for Einstein solid with N oscillators and q excitations"""
    return scipy.special.comb(N+q-1, q, exact=True)


t = Table(names=("q_A", "Omega_A", "q_B", "Omega_B", "Omega_{total}"),
          dtype=('int',)*5)

N_A = 6
N_B = 4
q_total = 6

print("N_A=%d, N_B=%d, q_total=%d\n" % (N_A, N_B, q_total))
print("Total number of microstates: %d" % multiplicity(N_A+N_B, q_total))

for q_A in range(0, q_total+1):
    omega_A = multiplicity(N_A, q_A)
    q_B = q_total-q_A
    omega_B = multiplicity(N_B, q_B)
    omega = omega_A*omega_B
    t.add_row((q_A, omega_A, q_B, omega_B, omega_A*omega_B,))
assert (sum(t["Omega_{total}"]) == multiplicity(N_A+N_B, q_total))

# write ascii table
ascii.write(t, format='fixed_width_two_line',
            delimiter_pad=' ', position_char='=')

# make "bar" graph:
plt.rcParams.update({"font.size": 12, "figure.figsize": (5, 3)})
fig = plt.figure()
ax = plt.subplot(111)
ax.bar(t['q_A'], t['Omega_{total}'], align="center")
plt.xlabel("$q_A$")
plt.ylabel("$\Omega_{total}$")
ax = plt.gca()
ax.xaxis.set_ticks(np.arange(0, q_total+1, 1))

# plt.show() (for interactive display)
plt.savefig("figure_generated.pdf", bbox_inches='tight')
